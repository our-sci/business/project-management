**REMEMBER! Add ~"decision" tag to this issue to mark it as a decision!**

What did you decide, what does it affect, and what should you reference for why you decided it?

Refresher: [How we do project management link](https://gitlab.com/groups/our-sci/-/wikis/Getting-Started)
